package com.tradevan.operate;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import us.codecraft.xsoup.Xsoup;

public class XSoupExample {

	public void run() throws IOException {
		String url = "http://www.foodstandards.gov.au/industry/foodrecalls/recalls/Pages/default.aspx";
		System.out.println(String.format("Fetching %s", url));

		Document doc = Jsoup.connect(url).get();
		Elements links = Xsoup
				.compile(
						"//div[@class='ms-WPBody noindex']/div/div/div/table/tbody/tr/td/h3/a")
				.evaluate(doc).getElements();

		System.out.println(String.format("\nLinks: (%d)", links.size()));
		for (Element link : links) {

			System.out.println(String.format(" * a: <%s>  (%s)",
					link.attr("abs:href"), link.text()));
		}
	}

}
