package com.tradevan.operate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.tradevan.bean.FanPageResult;
import com.tradevan.bean.ObjFQLResult;

public class CheckFanPage {

	public CheckFanPage() throws ClassNotFoundException {
		Class.forName(classForName);
	}

	private static final String conurl = "jdbc:edb://10.88.56.6:5444/crawl";

	private static final String classForName = "com.edb.Driver";

	private static final String user = "enterprisedb";

	private static final String pas = "caadm99z";

	public Map<String, FanPageResult> isExist(ObjFQLResult ofr, String table,
			String company) throws SQLException {
		System.out.println("===Checking Results===");

		Map<String, FanPageResult> nonExist = new HashMap<String, FanPageResult>();

		Connection con = DriverManager.getConnection(conurl, user, pas);

		PreparedStatement ps = null;

		boolean check = true;

		try {
			String sql = "select * from " + table
					+ " where urlpath=? and pagename=?";

			ps = con.prepareStatement(sql);

			// 判斷取得的粉絲頁是否存在於TABLE
			for (Map.Entry<String, FanPageResult> entry : ofr.table.entrySet()) {
				check = true;

				ps.setString(1, entry.getKey());
				ps.setString(2, entry.getValue().getPagename());

				// 若條件存在於TABLE，則以下不動作。
				ResultSet set = ps.executeQuery();
				while (set.next()) {
					check = false;
					break;
				}

				// 若粉絲頁不在TABLE內，則加入MAP，預備回傳和寫入TABLE
				if (check) {
					nonExist.put(entry.getKey(), entry.getValue());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();
			con.close();
		}

		// 若有TABLE內不存在的粉絲頁，則更新TABLE
		if (nonExist.size() > 0) {
			write(nonExist, table, company);
		}

		return nonExist;
	}

	private void write(Map<String, FanPageResult> nonExist, String table,
			String company) throws SQLException {
		System.out.println("===Writing new fanpage into Table===");
		Connection con = DriverManager.getConnection(conurl, user, pas);

		PreparedStatement ps = null;

		try {
			String sql = "insert into "
					+ table
					+ " (urlpath,pagename,company,fan_number,page_id) values(?,?,?,?,?)";

			ps = con.prepareStatement(sql);

			int count = 0;

			// 寫入更新的粉絲頁資訊
			for (Map.Entry<String, FanPageResult> entry : nonExist.entrySet()) {
				ps.setString(1, entry.getKey());
				ps.setString(2, entry.getValue().getPagename());
				ps.setString(3, company);
				ps.setInt(4, entry.getValue().getNumber());
				ps.setString(5, entry.getValue().getPageid());

				ps.addBatch();

				count++;

				if (count % 100 == 0) {
					ps.executeBatch();
					ps.clearBatch();
				}

			}
			// ps.executeBatch();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.executeBatch();
				ps.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
