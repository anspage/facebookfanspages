package com.tradevan.bean;

public class ObjCheckIn {

  private String placeID = "", placeName = "", street = "", latitude = "", longitude = "", created_time;

  public String getPlaceID() {
    return placeID;
  }

  public void setPlaceID(String placeID) {
    this.placeID = placeID;
  }

  public String getPlaceName() {
    return placeName;
  }

  public void setPlaceName(String placeName) {
    this.placeName = placeName;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getCreated_time() {
    return created_time;
  }

  public void setCreated_time(String created_time) {
    this.created_time = created_time;
  }
}
