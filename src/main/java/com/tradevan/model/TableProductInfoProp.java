package com.tradevan.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TableProductInfoProp {

	private String tablename = "", id = "", company = "", brand_name = "",
			common_name = "", description = "", size = "",
			code_on_product = "", item = "", case_upc = "", item_upc = "",
			packaging = "", cases = "", best_by_date = "";

	public TableProductInfoProp() {
		FileInputStream fis = null;
		try {
//			TEST
			fis = new FileInputStream(
					"src/main/resource/table_product_info.properties");
//			FORMAL
//			fis = new FileInputStream(
//			"C:/workspace/lib/src/main/resource/table_product_info.properties");
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String read_line;

			try {
				while ((read_line = br.readLine()) != null) {
					if (read_line.indexOf("#") == 0) {
						continue;
					}
					if (read_line.toLowerCase().indexOf("tablename:") >= 0) {
						this.tablename = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("id:") >= 0) {
						this.id = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("company:") >= 0) {
						this.company = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("brand_name:") >= 0) {
						this.brand_name = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("common_name:") >= 0) {
						this.common_name = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("description:") >= 0) {
						this.description = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("size:") >= 0) {
						this.size = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("code_on_product:") >= 0) {
						this.code_on_product = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("item:") >= 0) {
						this.item = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("case_upc:") >= 0) {
						this.case_upc = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("item_upc:") >= 0) {
						this.item_upc = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("packaging:") >= 0) {
						this.packaging = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("cases:") >= 0) {
						this.cases = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("best_by_date:") >= 0) {
						this.best_by_date = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getTablename() {
		return tablename;
	}

	public String getId() {
		return id;
	}

	public String getBrand_name() {
		return brand_name;
	}

	public String getCommon_name() {
		return common_name;
	}

	public String getDescription() {
		return description;
	}

	public String getSize() {
		return size;
	}

	public String getCode_on_product() {
		return code_on_product;
	}

	public String getItem() {
		return item;
	}

	public String getCase_upc() {
		return case_upc;
	}

	public String getItem_upc() {
		return item_upc;
	}

	public String getPackaging() {
		return packaging;
	}

	public String getCases() {
		return cases;
	}

	public String getBest_by_date() {
		return best_by_date;
	}

	public String getCompany() {
		return company;
	}

}
