package com.tradevan.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ParseDate {

	public ParseDate() {
	}

	public String formatDate(String sourcePattern, String targetPattern,
			String date, java.util.Locale sourceLocal,
			java.util.Locale targetLocal) {
		// targetPattern = "yyyy-MM-dd";
		// sourcePattern = "yyyy/MM/dd";
		// date = "2017/06/13";
		if(isValidDate(date,sourcePattern,sourceLocal)){
			Date d = new Date();

			SimpleDateFormat sdfSource = new SimpleDateFormat(sourcePattern,
					sourceLocal);
			SimpleDateFormat sdfTarget = new SimpleDateFormat(targetPattern,
					targetLocal);

			try {
				d = sdfSource.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return sdfTarget.format(d);
		}else{
			return date;
		}
		

	}
	
	public boolean isValidDate(String date, String sourcePattern, java.util.Locale sourceLocal) {
	    SimpleDateFormat sdfSource = new SimpleDateFormat(sourcePattern, sourceLocal);
	    try {
	    	sdfSource.parse(date);
	        return true;
	    } catch (ParseException e) {
	        return false;
	    }
	}

}
