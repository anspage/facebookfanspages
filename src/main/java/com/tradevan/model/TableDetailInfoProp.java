package com.tradevan.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TableDetailInfoProp {

	private String tablename = "", id = "", store_name = "", address = "",
			purchase_date = "";

	public TableDetailInfoProp() {
		FileInputStream fis = null;
		try {
//			TEST
			fis = new FileInputStream(
					"src/main/resource/table_detail_info.properties");
//			FORMAL
//			fis = new FileInputStream(
//			"C:/workspace/lib/src/main/resource/table_detail_info.properties");
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String read_line;

			try {
				while ((read_line = br.readLine()) != null) {
					if (read_line.indexOf("#") == 0) {
						continue;
					}
					if (read_line.toLowerCase().indexOf("tablename:") >= 0) {
						this.tablename = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("id:") >= 0) {
						this.id = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("store_name:") >= 0) {
						this.store_name = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("address:") >= 0) {
						this.address = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("purchase_date:") >= 0) {
						this.purchase_date = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getTablename() {
		return tablename;
	}

	public String getId() {
		return id;
	}

	public String getStore_name() {
		return store_name;
	}

	public String getAddress() {
		return address;
	}

	public String getPurchase_date() {
		return purchase_date;
	}

}
