package com.tradevan.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class QueryProp {

	private ArrayList<String> executeSQL = new ArrayList<String>();

	public QueryProp() {
		FileInputStream fis = null;
		try {
			// TEST
			fis = new FileInputStream("src/main/resource/query.properties");
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String read_line;

			try {
				while ((read_line = br.readLine()) != null) {
					if (read_line.indexOf("#") == 0) {
						continue;
					}
					this.executeSQL.add(read_line);

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public ArrayList<String> getExecuteSQL() {
		return executeSQL;
	}

}
