package com.tradevan.dbaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.tradevan.operate.AccessDatabase;
import com.tradevan.util.CeawlerConfig;
import com.tradevan.util.CrawlerConstant;

public class GetData {
	private static Logger logger = Logger.getLogger(GetData.class);

	public GetData() {
	}

	public List<String> getAllFansPageId() {
		String sql = "select fanspageid from committee_fanspage where fanspageid is not null";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		List<String> fansPageIdList = new ArrayList<String>();
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				String fanspageid = rs.getString("fanspageid");
				fansPageIdList.add(fanspageid);
			}
		} catch (Exception e) {
			logger.error("getAllFansPageId occur errors.", e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
		return fansPageIdList;
	}

	public List<String> getTopPostId() {
		String sql = "select distinct postid from facebook_fanspage_top10_post where postid is not null";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		List<String> fansPageIdList = new ArrayList<String>();
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				String fanspageid = rs.getString("postid");
				fansPageIdList.add(fanspageid);
			}
		} catch (Exception e) {
			logger.error("getAllFansPageId occur errors.", e);
		} finally {
			try {
				logger.debug("Success " + sql);
				con.close();
			} catch (SQLException e) {
			}
		}
		return fansPageIdList;
	}

	public void truncateTable(String tableName) {
		String sql = String.format("truncate table %s;", tableName);
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			ps.execute();
		} catch (Exception e) {
			logger.error("Truncate occur errors.", e);
		} finally {
			try {
				con.close();
				logger.debug("Success Truncate Table " + tableName);
			} catch (SQLException e) {
			}
		}
	}

	public void insertTopTen(String tableName, String period) {
		String sql = String.format("insert into facebook_fanspage_top10_post "
				+ "SELECT '' as pageid,postid,'' as userid,committee as username,create_date as createdtime,'' as messagetag_id,'' as messagetag_name,"
				+ "comment_message,create_date as update_time,`share`,`like`"
				+ ",comm as comment_count,score,%s as period " + "FROM n333.%s  " + " order by score desc limit 10",
				period, tableName);
		logger.debug(sql);
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			ps.execute();
		} catch (Exception e) {
			logger.error("Truncate occur errors.", e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
	}

	// deletePast30
	public void deletePast30(String days) {
		String sql = String.format("delete from facebook_fanspage_comment_reactions " + " where createdtime < '%s'",
				days);
		logger.debug(sql);
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			ps.execute();
		} catch (Exception e) {
			logger.error("Truncate occur errors.", e);
		} finally {
			try {
				logger.debug("Success " + sql);
				con.close();
			} catch (SQLException e) {
			}
		}
	}

	public ResultSet getDataBySQL(String sql) {
		PreparedStatement ps = null;
		ResultSet result = null;
		Connection con = null;
		logger.debug(sql);
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			result = ps.executeQuery();
		} catch (Exception e) {
			logger.error("getDataBySQL occur errors.", e);
		}

		return result;

	}

	public List<String> getFanPage(String init, String table) {

		PreparedStatement ps = null;
		ResultSet result = null;
		List<String> ganpages = new ArrayList<String>();

		String sql = "select fanspage from " + table;
		if (init.equals("1")) {
			sql += " where \"init\"='1'";
		}
		Connection con = null;
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(sql);
			result = ps.executeQuery();

			while (result.next()) {
				ganpages.add(result.getString(1));
			}

		} catch (Exception e) {
			logger.error("getDataBySQL occur errors.", e);
		}
		return ganpages;

	}

	public void execute(String sql) {
		PreparedStatement ps = null;
		Connection con = null;
		logger.debug(StringEscapeUtils.unescapeJava(sql));
		try {
			con = AccessDatabase.getConnection();
			ps = con.prepareStatement(StringEscapeUtils.unescapeJava(sql));
			ps.execute();
			con.close();
		} catch (Exception e) {
			logger.error("getDataBySQL occur errors.", e);
			return;
		}

	}
}
